export class HomepageLink {
    constructor(public text: string, public link: string, public image: string) {
        this.image = 'assets/images/' + this.image;
    }
}
